const controller = require('../controller/event')
const { verifyQueryParams } = require('../validator')


module.exports = app => {
    const router = require('express').Router()

    // Retrieve all events
    router.get('/', controller.findAll)

    app.use('/api/events', verifyQueryParams, router)
}
