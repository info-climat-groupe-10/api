const controller = require('../controller/subEvent')
// const { verifyDate } = require('../validator')


module.exports = app => {
    const router = require('express').Router()

    // Retrieve a single Marchand
    router.get('/', controller.findAll)

    router.get('/:subEventId', controller.findOne)


    app.use('/api/sub_events', router)
}
