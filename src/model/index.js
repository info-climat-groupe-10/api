const Sequelize = require('sequelize')
const confDb = require('./configDb')

let conf = confDb.development

if (process.env.NODE_ENV === 'development') {
    console.log('Development database selected')
}

const sequelize = new Sequelize(conf.DB, conf.USER, conf.PASSWORD, {
    host: conf.HOST,
	port: conf.PORT,
    dialect: conf.dialect,
    define: {
        timestamps: false
    },
    pool: {
        max: conf.pool.max,
        min: conf.pool.min,
        acquire: conf.pool.acquire,
        idle: conf.pool.idle
    }
})

const db = {}

db.Sequelize = Sequelize
db.sequelize = sequelize

const Event = require('./event')(sequelize, Sequelize)
const SubEvent = require('./subEvent')(sequelize, Sequelize)
// const Credentials = require('./credentials')(sequelize, Sequelize)
// const Options = require('./options')(sequelize, Sequelize)
// const Contact = require('./contact')(sequelize, Sequelize)

// Marchand.hasOne(Contact, {foreignKey: 'marchandId'})
// Marchand.hasOne(Options, {foreignKey: 'marchandId'})
// Marchand.hasOne(Credentials, {foreignKey: 'marchandId'})

// Marchand.addHook('afterCreate', m => {
//     return Promise.all([Credentials.create({marchandId: m.id}),
//         Options.create({marchandId: m.id}),
//         Contact.create({marchandId: m.id}),
//     ])
// })
//
db.Event = Event
db.SubEvent = SubEvent
// db.Contact = Contact
// db.Options = Options
// db.Credentials = Credentials

module.exports = db
