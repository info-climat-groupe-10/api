
module.exports = (sequelize, Sequelize) => {
    const SubEvent = sequelize.define('historic_values', {
        // attributes
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true,
        },
        id_historic: {
            type: Sequelize.INTEGER,
        },
        lieu: {
            type: Sequelize.STRING,
        },
        geoid: {
            type: Sequelize.INTEGER
        },
        dept: {
            type: Sequelize.STRING,
        },
        pays: {
            type: Sequelize.STRING,
        },
        valeur: {
            type: Sequelize.FLOAT,
        },
        type: {
            type: Sequelize.INTEGER,
        },
        date: {
            type: Sequelize.DATE
        },
        commentaire: {
            type: Sequelize.STRING
        },
        est_record: {
            type: Sequelize.INTEGER
        },
        est_record_dpt: {
            type: Sequelize.INTEGER
        }
    })

    return SubEvent
}
