
module.exports = (sequelize, Sequelize) => {
    const Event = sequelize.define('historic_events', {
        // attributes
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true,
        },
        nom: {
            type: Sequelize.STRING,
            allowNull: true
        },
        localisation: {
            type: Sequelize.STRING,
        },
        importance: {
            type: Sequelize.INTEGER
        },
        has_image_cyclone: {
            type: Sequelize.INTEGER,
        },
        date_deb: {
            type: Sequelize.DATE,
        },
        date_fin: {
            type: Sequelize.DATE,
        },
        duree: {
            type: Sequelize.STRING,
        },
        type: {
            type: Sequelize.STRING
        },
        description: {
            type: Sequelize.STRING
        },
        short_desc: {
            type: Sequelize.STRING
        },
        sources: {
            type: Sequelize.STRING
        },
        bs_link: {
            type: Sequelize.INTEGER
        },
        gen_cartes: {
            type: Sequelize.INTEGER
        },
        why: {
            type: Sequelize.STRING
        },
        tableau_croise: {
            type: Sequelize.STRING
        },
        hits: {
            type: Sequelize.INTEGER
        },
        notes: {
            type: Sequelize.STRING
        }
    })

    return Event
}
