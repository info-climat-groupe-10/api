module.exports = {
    development: {
        HOST: process.env.MYSQL_HOST || 'db',
	    PORT: process.env.MYSQL_PORT || '3306',
        USER: process.env.MYSQL_USER || 'infoclimat',
        PASSWORD: process.env.MYSQL_PASSWORD || 'infoclimat',
        DB: process.env.MYSQL_DB || 'infoclimat',
        dialect: 'mysql',
        pool: {
            max: 5,
            min: 0,
            acquire: 30000,
            idle: 10000
        }
    },
    test: {
	    HOST: process.env.MYSQL_HOST || 'db',
	    PORT: process.env.MYSQL_PORT || '3306',
	    USER: process.env.MYSQL_USER || 'infoclimat',
	    PASSWORD: process.env.MYSQL_PASSWORD || 'infoclimat',
	    DB: process.env.MYSQL_DB || 'infoclimat',
	    dialect: 'mysql',
	    pool: {
		    max: 5,
		    min: 0,
		    acquire: 30000,
		    idle: 10000
	    }
    }
}
