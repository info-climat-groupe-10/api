const validateDate = require("validate-date")

module.exports = {

    verifyQueryParams: async (req, res, next) => {

        const regions = {
            france: ["-1"],
            idf: ["91", "92", "93", "94", "95", "75", "77", "78"],
            auvergne_rhone_alpes: ["01", "03", "07", "15", "26", "38", "42", "43", "63", "69", "73", "74"],
            bourgogne_france_comte: ["21", "25", "39", "58", "70", "71", "89", "90"],
            bretagne: ["22", "29", "35", "56"],
            centre_val_de_loire: ["18", "28", "36", "37", "41", "45"],
            corse: ["2A", "2B"],
            grand_est: ["08", "10", "51", "52", "54", "55", "57", "67", "68", "88"],
            haut_de_france: ["02", "59", "60", "62", "80"],
            normandie: ["14", "27", "50", "61", "76"],
            nouvelle_aquitaine: ["16", "17", "19", "23", "24", "33", "40", "47", "64", "79", "86", "87"],
            occitanie: ["09", "11", "12", "30", "31", "32", "34", "46", "48", "65", "66", "81", "82"],
            pays_de_la_loire: ["44", "49", "53", "72", "85"],
            provence_alpes_cote_d_azur: ["04", "05", "06", "13", "83", "84"],
            guadeloupe: ["971"],
            martinique: ["972"],
            guyane: ["973"],
            la_reunion: ["974"],
            mayotte: ["976"],
            saint_barthelemy: ["977"],
            saint_martin:["978"],
            polynesie_francaise: ["987"],
            nouvelle_caledonie: ["988"]
        };

        try {
            const {date_deb} = req.query;
            const {date_fin} = req.query;
            const {duree} = req.query;
            const {importance} = req.query;
            const {type} = req.query;
            const {eventId} = req.query;
            const { localisation } = req.query;


            if(date_deb || date_fin) {

                if ((date_deb && !date_fin) || (!date_deb && date_fin)) {
                    res.status(406).send({
                        message: `Cannot retrieve events`,
                        error: `You must provide date_debut and date_fin`
                    })
                } else if(!validateDate(date_deb, responseType="boolean") && !validateDate(date_deb, responseType="boolean")) {
                    res.status(422).send({
                        message: `Cannot retrieve events`,
                        error: `Invalid date format`
                    })
                } else if(new Date(date_deb) > new Date(date_fin)) {
                    res.status(406).send({
                        message: `Cannot retrieve events`,
                        error: `date_debut cannot be greater than date_fin`
                    })
                }
            }
            // console.log(Number.isInteger(parseInt(duree)))
            // if (duree && !Number.isInteger(parseInt(duree))){
            //     console.log(duree)
            //
            //     return res.status(404).send({
            //         success: false,
            //         message: 'You must give an integer',
            //     })
            // }

            if((duree && !Number.isInteger(parseInt(duree))) || (importance && !Number.isInteger(parseInt(importance))) || (type && !Number.isInteger(parseInt(type))) || (eventId && !Number.isInteger(parseInt(eventId)))) {
                // console.log( (!type && !Number.isInteger(parseInt(type))))
                // console.log( (!duree && !Number.isInteger(parseInt(duree))))
                return res.status(404).send({
                    success: false,
                    message: 'You must give an integer',
                })
            }

            if (localisation) {
                if (!Object.keys(regions).includes(localisation)) {

                    return res.status(400).send({
                        success: false,
                        message: 'Localisation not found or wrong ',
                    })
                }
            }
        } catch (error) {
            return res.status(404).send({
                success: false,
                message: error.message || 'Something went wrong',
            })
        }
        next()

    },
    verifyParamsInt: (param) => {
        return Number.isInteger(parseInt(param))
    }
    // verifyParamsInt: async (req, res, next) => {
    //
    //
    //
    //     if((!duree || !importance || !type || !eventId ) && !Number.isInteger(parseInt(duree)) && !Number.isInteger(parseInt(importance)) && !Number.isInteger(parseInt(type)) && !Number.isInteger(parseInt(eventId))) {
    //         try {
    //             return res.status(404).send({
    //                 success: false,
    //                 message: 'You must give an integer',
    //             })
    //
    //         } catch (error) {
    //             return res.status(500).send({
    //                 success: false,
    //                 message: error.message || 'Something went wrong',
    //             })
    //         }
    //     }
    //     next()
    // },

    // verifyLocalisation: async (req, res, next) => {
    //
    //     const regions = {
    //         france: ["-1"],
    //         idf: ["91", "92", "93", "94", "95", "75", "77", "78"],
    //         auvergne_rhone_alpes: ["01", "03", "07", "15", "26", "38", "42", "43", "63", "69", "73", "74"],
    //         bourgogne_france_comte: ["21", "25", "39", "58", "70", "71", "89", "90"],
    //         bretagne: ["22", "29", "35", "56"],
    //         centre_val_de_loire: ["18", "28", "36", "37", "41", "45"],
    //         corse: ["2A", "2B"],
    //         grand_est: ["08", "10", "51", "52", "54", "55", "57", "67", "68", "88"],
    //         haut_de_france: ["02", "59", "60", "62", "80"],
    //         normandie: ["14", "27", "50", "61", "76"],
    //         nouvelle_aquitaine: ["16", "17", "19", "23", "24", "33", "40", "47", "64", "79", "86", "87"],
    //         occitanie: ["09", "11", "12", "30", "31", "32", "34", "46", "48", "65", "66", "81", "82"],
    //         pays_de_la_loire: ["44", "49", "53", "72", "85"],
    //         provence_alpes_cote_d_azur: ["04", "05", "06", "13", "83", "84"],
    //         guadeloupe: ["971"],
    //         martinique: ["972"],
    //         guyane: ["973"],
    //         la_reunion: ["974"],
    //         mayotte: ["976"],
    //         saint_barthelemy: ["977"],
    //         saint_martin:["978"],
    //         polynesie_francaise: ["987"],
    //         nouvelle_caledonie: ["988"]
    //     };
    //
    //     try {
    //
    //     } catch (error) {
    //         return res.status(404).send({
    //             success: false,
    //             message: error.message || 'Something went wrong',
    //         })
    //     }
    //     next()
    // }
}
