const { Event } = require('../model')
const { Op } = require("sequelize");
const { verifyLocalisation } = require("../validator")

module.exports = {
    findAll: async (req, res) => {

        try {
            const {date_deb} = req.query;
            const {date_fin} = req.query;
            const {localisation} = req.query;
            const {importance} = req.query;
            const {duree} = req.query;
            const {type} = req.query;

            const regions = {
                france: ["-1"],
                idf: ["91", "92", "93", "94", "95", "75", "77", "78"],
                auvergne_rhone_alpes: ["01", "03", "07", "15", "26", "38", "42", "43", "63", "69", "73", "74"],
                bourgogne_france_comte: ["21", "25", "39", "58", "70", "71", "89", "90"],
                bretagne: ["22", "29", "35", "56"],
                centre_val_de_loire: ["18", "28", "36", "37", "41", "45"],
                corse: ["2A", "2B"],
                grand_est: ["08", "10", "51", "52", "54", "55", "57", "67", "68", "88"],
                haut_de_france: ["02", "59", "60", "62", "80"],
                normandie: ["14", "27", "50", "61", "76"],
                nouvelle_aquitaine: ["16", "17", "19", "23", "24", "33", "40", "47", "64", "79", "86", "87"],
                occitanie: ["09", "11", "12", "30", "31", "32", "34", "46", "48", "65", "66", "81", "82"],
                pays_de_la_loire: ["44", "49", "53", "72", "85"],
                provence_alpes_cote_d_azur: ["04", "05", "06", "13", "83", "84"],
                guadeloupe: ["971"],
                martinique: ["972"],
                guyane: ["973"],
                la_reunion: ["974"],
                mayotte: ["976"],
                saint_barthelemy: ["977"],
                saint_martin:["978"],
                polynesie_francaise: ["987"],
                nouvelle_caledonie: ["988"]
            };

            let filters = [];

            if (date_deb && date_fin) {
                filters.push({
                    [Op.or]: [
                        {
                            date_deb: {
                                [Op.between]: [date_deb, date_fin]
                            }
                        },
                        {
                            date_fin: {
                                [Op.between]: [date_deb, date_fin]
                            }

                        },
                        {
                            [Op.and]: [
                                {
                                    date_deb: {
                                        [Op.lt]: date_deb
                                    }
                                },
                                {
                                    date_fin: {
                                        [Op.gt]: date_fin
                                    }
                                },
                            ]
                        },
                    ]
                })
            }

            if (localisation) {


               let sublocs = [];
                regions[localisation].map( dep =>
                    sublocs.push({
                        [Op.substring]: dep
                    })
                )

                filters.push({
                    localisation: {
                        [Op.or]: sublocs
                    }
                })
            }

            if (importance) {
                filters.push({
                    importance: {
                        [Op.eq]: parseInt(importance)
                    }
                });
            }

            if (duree) {
                filters.push({
                    importance: {
                        [Op.eq]: parseInt(duree)
                    }
                });
            }

            if (type) {

                let types = [];
                if (Array.isArray(type)) {
                    type.map( t =>
                        types.push({
                            [Op.regexp]: '(^'+type+'$|^'+type+',|,'+type+'$|,'+type+',)'
                        })
                    )
                } else {
                    types.push({
                        [Op.regexp]: '(^'+type+'$|^'+type+',|,'+type+'$|,'+type+',)'
                    })
                }

                filters.push({
                    type: {
                        [Op.or]: types
                    }
                });
            }

            const events = await Event.findAll({
                where: {
                    [Op.and]: filters
                }
            });
            res.send(events);

        } catch (error) {
            res.status(500).send({
                message: `Cannot retrieve all events`,
                error: error
            })
        }
    }
}