const { SubEvent } = require('../model')
const { Op } = require("sequelize");

module.exports = {

    findAll: async (req, res) => {
        try{
            let subEvents;

           const {eventId} = req.query;

            if (eventId) {
                subEvents = await SubEvent.findAll({
                    where: {
                        id_historic: {
                            [Op.eq]: parseInt(eventId)
                        }
                    }
                });
            } else {
                subEvents = await SubEvent.findAll();
            }

            res.send(subEvents);

        } catch (error) {
            res.status(500).send({
                message: `Cannot retrieve Sub Events`,
                error: error
            })
        }
    },

    findOne: async (req, res) => {
        try{
            const {subEventId} = req.params;

            if (Number.isInteger(parseInt(subEventId))){
                const subEvent = await SubEvent.findByPk(parseInt(subEventId));
                res.send(subEvent);
            } else {
                res.status(404).send({
                    error: `Sub event Id must be an integer`,
                })
            }
        } catch (error) {
            console.log(error)
            res.status(500).send({
                message: `Cannot retrieve Sub Events`,
                error: error
            })
        }
    }
}