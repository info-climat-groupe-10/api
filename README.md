# Présentation

Ce projet a été organisé dans le cadre du hackaton 5e année du [réseau GES](https://www.reseau-ges.fr/) ([ESGI](https://www.esgi.fr/) [ECITV](https://www.ecitv.fr/)), édtion 2021.  
Cette application a pour vocation d'exploiter à minima les données de l'association [Infoclimat](https://www.infoclimat.fr/).  
Il s'agit d'une API Rest, permettant seulement d'exploiter les données (seulement de la récupération via la méthode HTTP GET).

# Stack technique

Pour exploiter les données fourni par l'association, la stack suivante a été mise en place:

- [NodeJS](https://nodejs.org/en/): environnement de développement
- [Express](https://expressjs.com/fr/): Framework javascript
- [Sequelize](https://sequelize.org/):  ORM pour la gestion des données
- [MySQL](https://www.mysql.com/fr/): base de données pour le stockage des données
- [Docker](https://www.docker.com/): environnement de développement conteneurisé

# Installation
## Variables d'environnements
À mettre dans un fichier ```.env``` à la racine du dossier.
- ```MYSQL_HOST:``` Adresse de la base de données
- ```MYSQL_PORT:``` Port d'écoute de la base de données
- ```MYSQL_USER:``` Identifiant de la base de données
- ```MYSQL_PASSWORD:``` Mot de passe de la base de données
- ```MYSQL_DB:``` Nom de la base de données

## Commandes

- ```docker-compose build:``` Installation de l'environnement de développement et des dépendances
- ```docker-compose up:``` Lancement des containers docker
- ```docker-compose stop:``` Arrêt les containers

# Exploitation

Endpoint d'API:

### Récupération des "évènements globaux" : ```/api/events/```
Paramètres de requête optionnel:

- localisation  (nom région)
- date_debut  (aaaa-mm-dd)
- date_fin  (aaaa-mm-dd)
- importance  (id importance évènement)
- duree  (id duree évènement)
- type  (id type évènement)

Exemple: ```/api/events/?date_debut=2000-01-15&&date_fin=2002-08-11&location=idf&duree=2```

### Récupération des "sous évènements": ```/api/sub_events/```
Paramètres de requête optionnel:

- eventId (id de l'évènement global auquel il réfère)

Exemple: ```/api/sub_events/?eventId=25```

# Exemple
Exemple d'API déployé avec un jeu de données:
https://api-inflo-climat.herokuapp.com/api/events/
